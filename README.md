# sendkey

## About

This is a simple bash and AppleScript combo, which serves the sole purpose of sending a keypress to a specified applcation via the command line.

## Usage

The script takes 4 parameters:

    -a: the application in question
    -k: the key we are sending
    -n: number of repeats (1 by default)
    -m: the key modifiers applied
    -r: add the carriage return (send Enter)

The modifiers in question are Shift, Control, Option and Command keys, which are passed to the script in a form of binary number, representing their ON or OFF position. This means that `1001` equals to Shift and Command modifiers being used, while Control and Option don't.

This way command:

    sendkey -a Safari -k "[" -m 0001 -n 3

will send to the browser Safari the "Back" hotkey (`Cmd-[`) 3 times in a row, resulting in going back in history three times.

And this one:

    sendkey -a iTerm -k "d" -m 0001 && sendkey -a iTerm -k "ls -la" -r

will split the current iTerm's window vertically and execute `ls -la` in this new split.

Obviously, this script is intended to use only in connection with some other scripts, and not straightforward application could be seen as reasonable.
